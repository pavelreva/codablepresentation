import SwiftRepository

extension Country {
    static func getAll() -> MainRequestBuilder {
        return MainRequestBuilder(path: "countries")
    }
}
