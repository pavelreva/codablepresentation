import SwiftRepository
import PromiseKit
import RealmSwift

struct CountryRepository: Repository, Syncable, Storable {

    let remote: ObjectsStore<Country>
    let local: RealmStore<Country>

    static func `default`() -> CountryRepository {
        let remote = ObjectsStore<Country>(session: inject(), handler: inject())
        return CountryRepository(remote: remote, local: RealmStore(inject()))
    }
    
    func getAll() -> Promise<[Country]> {
        firstly {
            self.remote.requestArray(request: Country.getAll())
        }.then {
            self.local.save($0)
        }.then { _ in
            self.local.getItems()
        }
    }
    
    func getLocalItems() -> [Country] {
        self.local.getItems()
    }
}
