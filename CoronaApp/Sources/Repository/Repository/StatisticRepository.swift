import SwiftRepository
import PromiseKit
import RealmSwift

struct StatisticRepository: Repository, Syncable, Storable {

    let remote: ObjectsStoreDecodable<CommonStatistic>
    let local: RealmStore<CommonStatistic>

    static func `default`() -> StatisticRepository {
        let remote = ObjectsStoreDecodable<CommonStatistic>(session: inject(), handler: inject())
        return StatisticRepository(remote: remote, local: RealmStore(inject()))
    }
    
    func get() -> Promise<CommonStatistic> {
        firstly {
            self.remote.requestObject(request: CommonStatistic.get())
        }.then {
            self.local.save($0)
        }.then { _ in
            self.local.getItem()
        }
    }
    
    func getLocalItem() -> CommonStatistic? {
        self.local.getItem()
    }
}
