import UIKit

protocol MainScreenViewControllerOutput: ViewControllerOutput {
}

class MainScreenViewController: UIViewController {

    var output: MainScreenViewControllerOutput!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
        }
    }
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.start()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.updateData()
    }

    // MARK: - Private

    private func configureUI() {
        
    }
}

// MARK: - Private MainScreenViewModelOutput
extension MainScreenViewController: MainScreenViewModelOutput {

    func dataDidUpdate() {

    }
}
