import UIKit

class MainScreenConfigurator {

    class func configure(data: MainScreenViewModel.ModuleInput = MainScreenViewModel.ModuleInput(),
                         output: MainScreenViewModel.ModuleOutput? = nil) -> UIViewController {
        let viewController = R.storyboard.main.mainScreenViewController()!
        let viewModel = MainScreenViewModel(dependencies: createDependencies(), data: data)
            
        viewController.output = viewModel
        viewModel.output = viewController
        viewModel.moduleOutput = output
            
        return viewController
    }
    
    private class func createDependencies() -> MainScreenViewModel.Dependencies {
        return MainScreenViewModel.Dependencies()
    }
}
