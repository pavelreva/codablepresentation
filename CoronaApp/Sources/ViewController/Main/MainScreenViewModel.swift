import Foundation

protocol MainScreenViewModelOutput: ViewModelOutput {
    func dataDidUpdate()
}

class MainScreenViewModel {

    struct Dependencies { }
    
    let dependencies: Dependencies
    
    let moduleInput: ModuleInput
    var moduleOutput: ModuleOutput?
    
    weak var output: MainScreenViewModelOutput!

    init(dependencies: Dependencies, data: ModuleInput) {
        self.dependencies = dependencies
        self.moduleInput = data
    }
}

// MARK: - MainScreenViewControllerOutput
extension MainScreenViewModel: MainScreenViewControllerOutput {
    
    func start() {
        
    }

    func update() {
        output.dataDidUpdate()
    }
}
