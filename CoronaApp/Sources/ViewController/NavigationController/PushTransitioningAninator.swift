import UIKit

class PushTransitioningAninator: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.35
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        let frame = transitionContext.finalFrame(for: toController)

        let fromOffset = frame.offsetBy(dx: 0, dy: frame.size.height)

        toController.view.frame = fromOffset

        let toOffset = frame.offsetBy(dx: 0, dy: -frame.size.height + 120)

        transitionContext.containerView.insertSubview(toController.view, aboveSubview: fromController.view)

        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                fromController.view.frame = toOffset
                toController.view.frame = frame
        }, completion: {_ in
            transitionContext.completeTransition(true)
        })
    }
}

class PopTransitioningAninator: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.35
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        let frame = transitionContext.initialFrame(for: fromController)

        let toOffsetPop = frame.offsetBy(dx: 0, dy: -frame.size.height + 120)

        toController.view.frame = toOffsetPop

        let fromOffsetPop = frame.offsetBy(dx: 0, dy: frame.size.height)

        transitionContext.containerView.insertSubview(toController.view, belowSubview: toController.view)

        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                toController.view.frame = frame
                fromController.view.frame = fromOffsetPop
        }, completion: {_ in
            transitionContext.completeTransition(true)
        })
    }
}
