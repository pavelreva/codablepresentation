import UIKit

struct Empty: Decorator {
    
    func decorate(_ navigationBar: UINavigationBar) {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.backgroundColor = .clear
        navigationBar.tintColor = .clear
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.clear]
    }
    
    func decorateBackButton(for viewController: UIViewController, selector: Selector) {
        viewController.navigationItem.hidesBackButton = true
    }
}

struct Blue: Decorator {
    
    func decorate(_ navigationBar: UINavigationBar) {
        //navigationBar.setBackgroundImage(UIImage.genarate(color: Colors.darkSlateBlue), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.tintColor = .white
        //navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white, .font: Fonts.semiBold(18)]
    }
    
    func decorateBackButton(for viewController: UIViewController, selector: Selector) {
        let backButton = UIBarButtonItem(image: UIImage(named: "back"),
                                         style: .plain,
                                         target: viewController.navigationController,
                                         action: selector)
        
        viewController.navigationItem.leftBarButtonItem = backButton
        viewController.navigationItem.hidesBackButton = true
    }
}
