import UIKit

class MainNavigationController: NavigationController {

    init(nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, decorator: Blue())
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, decorator: Blue())
    }
}

class LoginNavigationController: NavigationController {

    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, decorator: Empty())
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, decorator: Empty())
    }
}
