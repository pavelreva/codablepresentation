import Foundation

extension LaunchViewModel {

    enum ModuleOutputAction {
        case launchFinished
    }

    struct ModuleInput {

    }

    struct ModuleOutput {
        let action: (ModuleOutputAction) -> Void
    }

}
