import Lottie

protocol LaunchViewControllerOutput: ViewControllerOutput {

}

class LaunchViewController: UIViewController {

    var output: LaunchViewControllerOutput!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var animationContainer: UIView!
    private lazy var animationView: AnimationView = {
        let animationView = AnimationView(frame: CGRect(origin: .zero, size: .zero))
        return animationView
    }()
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.start()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.updateData()
    }

    // MARK: - Private

    private func configureUI() {
        titleLabel.text = R.string.localizable.label_splash_title()
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationContainer.addSubview(animationView)
        animationView.topAnchor.constraint(equalTo: animationContainer.topAnchor).isActive = true
        animationView.leadingAnchor.constraint(equalTo: animationContainer.leadingAnchor).isActive = true
        animationView.bottomAnchor.constraint(equalTo: animationContainer.bottomAnchor).isActive = true
        animationView.trailingAnchor.constraint(equalTo: animationContainer.trailingAnchor).isActive = true
    }
}

// MARK: - Private LaunchViewModelOutput
extension LaunchViewController: LaunchViewModelOutput {

    func startActivity() {
        let animation = Animation.named("loadingAnimation")
        animationView.animation = animation
        animationView.loopMode = .loop
        animationView.play()
    }
    
    func stopActivity() {
        animationView.stop()
    }

    func dataDidUpdate() { }
}
