import PromiseKit

protocol LaunchViewModelOutput: ViewModelOutput {

    func dataDidUpdate()
}

class LaunchViewModel {

    struct Dependencies {
        let synchManager: SynchInteractor
    }

    let dependencies: Dependencies
    
    let moduleInput: ModuleInput
    var moduleOutput: ModuleOutput?
    
    weak var output: LaunchViewModelOutput!

    init(dependencies: Dependencies, data: ModuleInput) {
        self.dependencies = dependencies
        self.moduleInput = data
    }
}

// MARK: - LaunchViewControllerOutput
extension LaunchViewModel: LaunchViewControllerOutput {

    func start() {
        
    }

    func updateData() {
        output.startActivity()
        dependencies.synchManager.synch().asVoid()
        .then {
            after(seconds: 1.5)
        }.done { [weak self] in
            self?.moduleOutput?.action(.launchFinished)
        }.ensure { [weak self] in
            self?.output.stopActivity()
        }.catch { [weak self] in
            self?.output.catchError($0)
        }
    }
}
