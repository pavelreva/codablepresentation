import UIKit

class LaunchConfigurator {

    class func configure(data: LaunchViewModel.ModuleInput = LaunchViewModel.ModuleInput(),
                         output: LaunchViewModel.ModuleOutput? = nil) -> UIViewController {
        let viewController = R.storyboard.main.launchViewController()!
        let viewModel = LaunchViewModel(dependencies: createDependencies(), data: data)
            
        viewController.output = viewModel
        viewModel.output = viewController
        viewModel.moduleOutput = output
            
        return viewController
    }
    
    private class func createDependencies() -> LaunchViewModel.Dependencies {
        return LaunchViewModel.Dependencies(synchManager: inject())
    }
}
