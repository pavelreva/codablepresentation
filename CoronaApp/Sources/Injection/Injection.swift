import UIKit

func registerService<T>(service: T) {
    ServiceLocator.shared.registerService(service: service)
}

func inject<T>() -> T {
    return ServiceLocator.shared.getService()
}
