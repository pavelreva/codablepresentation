import UIKit

protocol DependenceProviders {
    
    func execute()
}
