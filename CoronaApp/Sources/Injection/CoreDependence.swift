import SwiftRepository
import Alamofire
import RealmSwift

class CoreDependence: DependenceProviders {

    let window: UIWindow?
    
    init(_ window: UIWindow?) {
        self.window = window
    }
    
    func execute() {
        let realm = store()
        let defaultSession: DefaultSessionManager = DefaultSessionManager.default()
        let mainSession: Session = MainSessionManager.default()
        let loger: Log = API.Configuration.current.loger
        let handler: BaseHandler = BaseHandler(loger)
        let synchService: SynchInteractor = SynchInteractor()
        
        registerService(service: realm)
        registerService(service: defaultSession)
        registerService(service: mainSession)
        registerService(service: loger)
        registerService(service: handler)
        registerService(service: synchService)
        
        let countryRepository = CountryRepository.default()
        let statisticRepository = StatisticRepository.default()
        registerService(service: countryRepository)
        registerService(service: statisticRepository)
    }

    private func store() -> Realm {
        do {
            return try Realm()
        } catch _ {
            try? FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
            return store()
        }
    }
}
