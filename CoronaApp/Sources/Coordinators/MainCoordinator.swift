import Foundation

class MainCoordinator: NavigationCoordinator {
    struct Output {
        let authorized: () -> Void
    }

    var output: Output?
    
    override func start() {
        let mainScreen = MainScreenConfigurator.configure(data: MainScreenViewModel.ModuleInput(), output: MainScreenViewModel.ModuleOutput(action: { action in
            print("action")
        }))
        set([mainScreen])
    }
}
