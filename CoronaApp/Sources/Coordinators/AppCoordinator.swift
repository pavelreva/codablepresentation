import UIKit

class AppCoordinator: WindowCoordinator {

    override func start() {
        configure()
    }
    
    private func configure() {
        self.setRoot(viewControler: UIViewController())
        presentLaunch()
    }
    
    private func presentLaunch() {
        removeAllChilds()
        let output = LaunchViewModel.ModuleOutput { [weak self] action in
            switch action {
            case .launchFinished:
                self?.presentMainFlow()
            }
        }
        let launch = LaunchConfigurator.configure(output: output)
        setRoot(viewControler: launch)
    }
        
    private func presentMainFlow() {
        removeAllChilds()
        let navigationController = NavigationController(nibName: nil, bundle: Bundle.main, decorator: Empty())
        navigationController.isNavigationBarHidden = true
        let coordinator = MainCoordinator(container: navigationController)
        addChild(coordinator)
        coordinator.start()
        setRoot(viewControler: coordinator.container)
    }
}
