import ObjectMapper
import AFDateHelper

class DateTransformator: TransformType {

    public typealias Object = Date
    public typealias JSON = String

    public init() {}

    open func transformFromJSON(_ value: Any?) -> Date? {

        if let dateStr = value as? String {
            return Date(fromString: dateStr, format: .isoDateTimeMilliSec)
        }

        return nil
    }

    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return date.toString(format: .isoDateTimeMilliSec)
        }
        return nil
    }
}
