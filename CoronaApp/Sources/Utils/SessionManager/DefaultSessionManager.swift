import Alamofire

class DefaultSessionManager: Session {

    static func `default`() -> DefaultSessionManager {
        let configuration = URLSessionConfiguration.default
        //configuration.httpAdditionalHeaders = HTTPHeaders.default
        return DefaultSessionManager(configuration: configuration)
    }
}
