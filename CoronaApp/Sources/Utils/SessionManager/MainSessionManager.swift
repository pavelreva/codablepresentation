import Alamofire
import PromiseKit

struct Keys {
    static let authorization = "Authorization"
    static let accept = "Accept"
    static let contentType = "Content-Type"
}

struct DefaultHTTPHeaders {

    static var main: HTTPHeaders { var header = self.default
        header[Keys.accept] = "application/json"
        header[Keys.contentType] = "application/json"
        return header
    }

    static let `default` = HTTPHeaders.default

    static func adapt(_ urlRequest: URLRequest) -> URLRequest {
        var urlRequest = urlRequest

//        main.forEach {
//            if let headers = urlRequest.allHTTPHeaderFields, headers[$0.key] == nil {
//                urlRequest.setValue($0.value, forHTTPHeaderField: $0.key)
//            }
//        }
        return urlRequest
    }
}

class MainSessionManager: Session {

    static func `default`(_ adapter: RequestAdapter? = nil, _ retrier: RequestRetrier? = nil) -> MainSessionManager {
        let session = MainSessionManager(configuration: URLSessionConfiguration.default)
//        session.adapter = adapter
//        session.retrier = retrier
        return session
    }
}
