import UIKit
import SwiftRepository

struct API {

    enum Configuration: String {

        static var current: Configuration {
            return .development

//            #if DEVELOPMENT
//            return .development
//            #elseif ACCEPTANCE
//            return .acceptance
//            #elseif PRODUCTION
//            return .production
//            #endif
        }

        case development
        case acceptance
        case production

        var baseURL: String {
            switch self {
            case .development:
                return "http://private-anon-3ec930f889-valtechmobilebaselineapi.apiary-mock.com"
            case .acceptance:
                return "http://private-anon-3ec930f889-valtechmobilebaselineapi.apiary-mock.com"
            case .production:
                return "http://private-anon-3ec930f889-valtechmobilebaselineapi.apiary-mock.com"
            }
        }

        var loger: Log {
            switch self {
            case .development: return DEBUGLog()
            case .acceptance: return DEBUGLog()
            case .production: return RELEASELog()
            }
        }
    }

    static let exchange = ""
    static let members = "/team/members/"
}
