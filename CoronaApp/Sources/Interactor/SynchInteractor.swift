import PromiseKit

class SynchInteractor {
    
    private lazy var countriesRepository: CountryRepository = inject()
    private lazy var statisticRepository: StatisticRepository = inject()
    
    @discardableResult
    func synch() -> Promise<([Country], CommonStatistic?)> {
        let promises = [statisticRepository.get().asVoid(),
                        countriesRepository.getAll().asVoid()]
        return when(resolved: promises)
                .then { _ in
                    Promise.value((self.countriesRepository.getLocalItems(), self.statisticRepository.getLocalItem()))
        }
    }
}
