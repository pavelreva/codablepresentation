import PromiseKit
import SwiftRepository

class CountriesInteractor: Interactor {
    private lazy var repo: CountryRepository = inject()
    
    func getAll() -> Promise<[Country]> {
        repo.getAll()
    }
}
