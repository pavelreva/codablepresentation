import UIKit

class KeyboardObservable: UIViewController {

    @IBOutlet weak internal var bottomContraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOutside(_:)))
        view.addGestureRecognizer(tapGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.didShow(sender:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.didHide(sender:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.didShow(sender:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }

    @IBAction private func tapOutside(_ sender: UIView) {
        view.endEditing(true)
    }

    @objc internal func didShow(sender: NSNotification) {
        if let kbSize = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let duration = sender.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let curveNumber = sender.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber {
            let height = kbSize.size.height
            let curve = UIView.AnimationOptions(rawValue: UInt(curveNumber.intValue << 16))

            bottomContraint.constant = height

            UIView.animate(withDuration: duration, delay: 0, options: curve, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in })
        }
    }

    @objc internal func didHide(sender: NSNotification) {
        if let duration = sender.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let curveNumber = sender.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber {
            let curve = UIView.AnimationOptions(rawValue: UInt(curveNumber.intValue << 16))

            bottomContraint.constant = 0

            UIView.animate(withDuration: duration, delay: 0, options: curve, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in })
        }
    }
}
