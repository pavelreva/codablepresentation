import ObjectMapper
import RealmSwift
import ObjectMapperAdditions

class CommonStatistic: Object, Codable {
    @objc dynamic var updatedAt: Int = 0
    @objc dynamic var cases: Int = 0
    @objc dynamic var deaths: Int = 0
    @objc dynamic var recovered: Int = 0

    override class func primaryKey() -> String? {
        return #keyPath(updatedAt)
    }
}
