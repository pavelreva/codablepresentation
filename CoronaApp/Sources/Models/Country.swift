//
//  Country.swift
//  CoronaApp
//
//  Created by Pavlo Reva on 24/03/2020.
//  Copyright © 2020 Paletech. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapperAdditions

class Country: Object, Mappable {
    
    @objc dynamic var country: String = ""
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
    @objc dynamic var flagPath: String = ""
    @objc dynamic var cases: Int = 0
    @objc dynamic var recovered: Int = 0
    @objc dynamic var active: Int = 0
    @objc dynamic var deaths: Int = 0

    override class func primaryKey() -> String? {
        return #keyPath(country)
    }

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        country <- map["country"]
        latitude <- map["countryInfo.lat"]
        longitude <- map["countryInfo.long"]
        flagPath <- map["countryInfo.flag"]
        cases <- map["cases"]
        recovered <- map["recovered"]
        active <- map["active"]
        deaths <- map["deaths"]
    }
}
