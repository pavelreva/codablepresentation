import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private lazy var coreDependence = CoreDependence(self.window)
    private var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        coreDependence.execute()
        
        appCoordinator = AppCoordinator(container: window!)
        appCoordinator.start()
        
        window?.makeKeyAndVisible()
        
        return true
    }
}
