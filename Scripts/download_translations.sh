#!/bin/bash
set -e

echo cd.

curl "https://localise.biz/api/export/locale/en.strings?key=eOS2qnnmTNukhuJGZLYHjU_FFE1X2pUO&filter=ios" | iconv -f utf-16 -t utf-8 > "./CoronaApp/Resources/Strings/en.lproj/Localizable.strings"

curl "https://localise.biz/api/export/locale/en.strings?key=eOS2qnnmTNukhuJGZLYHjU_FFE1X2pUO&filter=info_plist" | iconv -f utf-16 -t utf-8 > "./CoronaApp/en.lproj/InfoPlist.strings"

sed -i '' '/* Exported at:/d' "./CoronaApp/Resources/Strings/en.lproj/Localizable.strings"
sed -i '' '/* Exported at:/d' "./CoronaApp/en.lproj/InfoPlist.strings"
